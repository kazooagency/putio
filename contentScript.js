function reactToKeyDown(e) {
  if (e.code == "KeyF") {
    document.getElementsByTagName("video")[0].requestFullscreen();
  }
  if (e.code == "KeyK") {
    if (document.getElementsByTagName("video")[0].paused) {
      document.getElementsByTagName("video")[0].play();
    } else {
      document.getElementsByTagName("video")[0].pause();
    }
  }
}

document.addEventListener("keydown", reactToKeyDown);
